import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.util.StdDateFormat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Main {

    public static void main(String[] args) throws Exception {

        if (args.length != 2) {
            System.out.println("Required arguments: gkeep-notes-input.json standard-notes-output.json");
            return;
        }

        Path inputFilePath = Paths.get(args[0]);
        Path outputFilePath = Paths.get(args[1]);

        System.out.println("Deserialize JSON");

        ObjectMapper objectMapper = JsonMapper.builder()
                .findAndAddModules()
                .build()
                .setSerializationInclusion(JsonInclude.Include.NON_ABSENT)
                .setDateFormat(StdDateFormat.instance);

        Queue<JsonNode> flatNodes = new ArrayDeque<>();
        JsonNode rootJsonNode = objectMapper.readTree(Files.readAllBytes(inputFilePath));
        rootJsonNode.get("nodes").elements().forEachRemaining(flatNodes::add);

        System.out.println("Deserialized JSON to " + flatNodes.size() + " nodes");

        System.out.println("Build tree");

        TreeNode<JsonNode> treeRoot = TreeNode.createRoot();

        int iteration = 0;
        while (!flatNodes.isEmpty()) {

            iteration += 1;

            JsonNode node = flatNodes.remove();

            String parentId = node.get("parentId").asText();
            TreeNode<JsonNode> parent =
                    treeRoot.findSelfOrDescendantMatching(jsonNode -> jsonNode.get("id").asText().equals(parentId))
                            .orElse(treeRoot);

            assert !(parent == treeRoot && !parentId.equals("root")); // check for orphan nodes

            parent.addChild(node);
        }

        System.out.println("Built tree");
        System.out.println("Iterations: " + iteration);
        System.out.println("Max depth: " + treeRoot.maxDepth());
        System.out.println("Max width: " + treeRoot.maxWidth(true));

        System.out.println("Parse tree");

        TreeNode<GKeepNode> parsedTreeRoot = treeRoot.convertSelfAndDescendants(GKeepNode::new);

        System.out.println("Parsed tree");

        // Print parsed tree
        //System.out.println(parsedTreeRoot.printSelfAndDescendants());

        System.out.println("Parent nodes: " + parsedTreeRoot.children.size());
        System.out.println("Child  nodes: " + parsedTreeRoot.children.stream().mapToLong(c -> c.children.size()).sum());

        // tags (labels)

        List<GKeepLabel> gKeepLabels = getLabels(rootJsonNode);

        System.out.println(String.format("Found %d labels:", gKeepLabels.size()) + "\n" +
                gKeepLabels.stream()
                        .map(label -> label.name + " (" + label.mainId + ")")
                        .collect(Collectors.joining("\n"))
                        .indent(8).stripTrailing());

        // sanity checks

        checkEmptyNodes(parsedTreeRoot);
        checkNodesWithLabels(parsedTreeRoot);
        checkTitlePresence(parsedTreeRoot);
        checkLabelPresence(parsedTreeRoot);
        checkContents(parsedTreeRoot);
        checkTimestamps(parsedTreeRoot);

        // TODO: filter trashed notes
        // no strict need to: trashed is always 1970-01-01, assuming never

        // convert to common format

        List<Note> notes = parsedTreeRoot.children.stream()
                .map(parentNode -> new Note(parentNode, gKeepLabels))
                .collect(Collectors.toList());

        // convert to Standard Notes format

        List<StdItem> stdTags = StdConverter.convertLabels(gKeepLabels);
        List<StdItem> stdNotes = StdConverter.convertNotes(notes, stdTags);

        System.out.println("Converted tags (ex labels): " + stdTags.size());
        System.out.println("Converted notes: " + stdNotes.size());

        StdBackup stdBackup = new StdBackup(stdTags, stdNotes);

        objectMapper.setDefaultPrettyPrinter(new StdPrettyPrinter());

        String output = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(stdBackup);

        Files.writeString(outputFilePath, output, StandardCharsets.UTF_8);

        System.out.println("Done");
    }

    static List<GKeepLabel> getLabels(JsonNode rootJsonNode) {

        return StreamSupport.stream(rootJsonNode.get("labels").spliterator(), false)
                .map(GKeepLabel::new)
                .collect(Collectors.toList());
    }

    static void checkEmptyNodes(TreeNode<GKeepNode> root) {

        List<GKeepNode> emptySecondLevelNodes =
                root.children.stream()
                        .flatMap(firstLevelNode -> firstLevelNode.children.stream())
                        .map(TreeNode::getValue)
                        .filter(GKeepNode::appearsEmpty)
                        .collect(Collectors.toList());

        System.out.println("Empty child nodes: " + emptySecondLevelNodes.size());
        if (!emptySecondLevelNodes.isEmpty()) {
            System.out.println("Please review these manually:");
            emptySecondLevelNodes.forEach(node ->
                    System.out.println(node.id.indent(8).stripTrailing()));
        }
    }

    static void checkNodesWithLabels(TreeNode<GKeepNode> root) {

        List<TreeNode<GKeepNode>> nodesWithLabels =
                root.children.stream()
                        .filter(firstLevelChild -> firstLevelChild.findSelfOrDescendantMatching(node -> !node.labelIds.isEmpty()).isPresent())
                        .collect(Collectors.toList());
        System.out.println("Subtrees with labels: " + nodesWithLabels.size());
        //nodesWithLabels.forEach(node ->
        //        System.out.println(node.printSelfAndDescendants().indent(8)));
    }

    static void checkTitlePresence(TreeNode<GKeepNode> gkTreeRoot) {

        boolean titlesIn1stLevelNodes = gkTreeRoot.children.stream()
                .map(TreeNode::getValue)
                .map(GKeepNode::getTitle)
                .anyMatch(Predicate.not(Objects::isNull));

        System.out.println("Found titles in parent nodes: " + titlesIn1stLevelNodes);

        boolean titlesIn2ndLevelNodes = gkTreeRoot.children.stream()
                .map(TreeNode::getChildren)
                .flatMap(Collection::stream)
                .map(TreeNode::getValue)
                .map(GKeepNode::getTitle)
                .anyMatch(Predicate.not(Objects::isNull));

        System.out.println("Found titles in child  nodes: " + titlesIn2ndLevelNodes);

        assert !titlesIn2ndLevelNodes;
    }

    static void checkLabelPresence(TreeNode<GKeepNode> gkTreeRoot) {

        // find out if labels are only applied to 1st level nodes

        boolean labelsIn1stLevelNodes = gkTreeRoot.children.stream()
                .map(TreeNode::getValue)
                .map(GKeepNode::getLabelIds)
                .anyMatch(Predicate.not(List::isEmpty));

        System.out.println("Found labels in parent nodes: " + labelsIn1stLevelNodes);

        boolean labelsIn2ndLevelNodes = gkTreeRoot.children.stream()
                .map(TreeNode::getChildren)
                .flatMap(Collection::stream)
                .map(TreeNode::getValue)
                .map(GKeepNode::getLabelIds)
                .anyMatch(Predicate.not(List::isEmpty));

        System.out.println("Found labels in child  nodes: " + labelsIn2ndLevelNodes);

        assert !labelsIn2ndLevelNodes;
    }

    static void checkContents(TreeNode<GKeepNode> gkTreeRoot) {

        Collection<GKeepNode.ContentType> contentTypesFromParents = gkTreeRoot.children.stream()
                .map(TreeNode::getValue)
                .map(GKeepNode::getContentTypes)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        System.out.println("Content types found in parent nodes: " + contentTypesFromParents);

        Collection<GKeepNode.ContentType> contentTypesFromChildren = gkTreeRoot.children.stream()
                .map(TreeNode::getChildren)
                .flatMap(Collection::stream)
                .map(TreeNode::getValue)
                .map(GKeepNode::getContentTypes)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        System.out.println("Content types found in child  nodes: " + contentTypesFromChildren);

        List<TreeNode<GKeepNode>> childrenWithMultipleContentTypes =
                gkTreeRoot.children.stream()
                        .filter(parent ->
                                parent.children.stream()
                                        .map(TreeNode::getValue)
                                        .map(GKeepNode::getContentTypes)
                                        .map(Collection::size)
                                        .anyMatch(size -> size > 1))
                        .collect(Collectors.toList());

        System.out.println("Parents with one or more children having more than one content type: " +
                childrenWithMultipleContentTypes.size());
        childrenWithMultipleContentTypes.forEach(node ->
                System.out.println(node.printSelfAndDescendants().indent(8)));

        assert childrenWithMultipleContentTypes.size() == 0;

        List<TreeNode<GKeepNode>> parentsWithMoreThanOneTextChild = gkTreeRoot.children.stream()
                .filter(parent ->
                        parent.children.stream()
                                .map(TreeNode::getValue)
                                .map(GKeepNode::getText)
                                .filter(Predicate.not(Objects::isNull)).count() > 1)
                .collect(Collectors.toList());

        System.out.println("Parents with more than one text children: " + parentsWithMoreThanOneTextChild.size());

        //assert parentsWithMoreThanOneTextChild.size() == 0; // they appear to be checklists
    }

    static void checkTimestamps(TreeNode<GKeepNode> gkTreeRoot) {

        // any parents without updated date?

        boolean parentsWithoutUpdateDate = gkTreeRoot.children.stream()
                .map(TreeNode::getValue)
                .map(GKeepNode::getUpdated)
                .anyMatch(Objects::isNull);

        System.out.println("Parents  missing update date: " + parentsWithoutUpdateDate);

        // any children without updated date?

        boolean childrenWithoutUpdateDate = gkTreeRoot.children.stream()
                .map(TreeNode::getChildren)
                .flatMap(Collection::stream)
                .map(TreeNode::getValue)
                .map(GKeepNode::getUpdated)
                .anyMatch(Objects::isNull);

        System.out.println("Children missing update date: " + childrenWithoutUpdateDate);

        // any children modified after their parents? (are parents updated together with their children?)

        boolean childrenModifiedAfterTheirParent = gkTreeRoot.children.stream()
                .anyMatch(parent ->
                        parent.value.updated.isBefore(
                                parent.children.stream()
                                        .map(TreeNode::getValue)
                                        .map(GKeepNode::getUpdated)
                                        .max(OffsetDateTime::compareTo)
                                        .orElse(OffsetDateTime.MIN)));

        System.out.println("Children modified after  their parent: " + childrenModifiedAfterTheirParent);

        assert !childrenModifiedAfterTheirParent;

        boolean childrenModifiedBeforeTheirParent = gkTreeRoot.children.stream()
                .anyMatch(parent ->
                        parent.value.updated.isAfter(
                                parent.children.stream()
                                        .map(TreeNode::getValue)
                                        .map(GKeepNode::getUpdated)
                                        .max(OffsetDateTime::compareTo)
                                        .orElse(OffsetDateTime.MIN)));

        System.out.println("Children modified before their parent: " + childrenModifiedBeforeTheirParent);
    }

    static class StdPrettyPrinter extends DefaultPrettyPrinter {

        @Override
        public DefaultPrettyPrinter createInstance() {

            StdPrettyPrinter stdpp = new StdPrettyPrinter();
            stdpp._arrayIndenter = new DefaultIndenter();
            stdpp._objectFieldValueSeparatorWithSpaces = ": ";

            return stdpp;
        }

        @Override
        public void writeEndArray(JsonGenerator g, int nrOfValues) throws IOException {

            if (!_arrayIndenter.isInline()) {
                --_nesting;
            }
            if (nrOfValues > 0) {
                _arrayIndenter.writeIndentation(g, _nesting);
            } /*else {
                g.writeRaw(' ');
            }*/
            g.writeRaw(']');
        }
    }
}
