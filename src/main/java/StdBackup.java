import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
public class StdBackup {

    final String version = "004";

    List<StdItem> items = new ArrayList<>();

    StdBackup(List<StdItem> tags, List<StdItem> notes) {

        this.items = Stream.of(tags, notes).flatMap(Collection::stream).collect(Collectors.toList());
    }
}
