import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Data
public class GKeepNode {

    enum NodeType {NOTE, LIST, LIST_ITEM, BLOB}

    enum BlobType {AUDIO, IMAGE, DRAWING}

    enum ColorValue {DEFAULT, RED, ORANGE, YELLOW, GREEN, TEAL, BLUE, CERULEAN, PURPLE, PINK, BROWN, GRAY}

    enum CategoryValue {BOOKS, FOOD, MOVIES, MUSIC, PLACES, QUOTES, TRAVEL, TV}

    String id;

    String title;

    String text;

    Boolean isArchived;

    OffsetDateTime created;

    OffsetDateTime trashed;

    OffsetDateTime updated;

    OffsetDateTime userEdited;

    List<String> labelIds;

    Boolean checked;

    // blobs as text

    List<String> webLinks;

    String imageInfo;

    String drawingInfo;

    GKeepNode(JsonNode jsonNode) {

        this.id = jsonNode.get("id").asText();
        this.title = Optional.ofNullable(jsonNode.get("title")).map(JsonNode::asText).filter(Predicate.not(String::isBlank)).orElse(null);
        this.text = Optional.ofNullable(jsonNode.get("text")).map(JsonNode::asText).filter(Predicate.not(String::isBlank)).orElse(null);
        this.isArchived = Optional.ofNullable(jsonNode.get("isArchived")).map(JsonNode::asBoolean).orElse(null);
        this.created = Optional.ofNullable(jsonNode.get("timestamps")).flatMap(timestamps -> Optional.ofNullable(timestamps.get("created"))).map(JsonNode::asText).map(OffsetDateTime::parse).orElse(null);
        this.trashed = Optional.ofNullable(jsonNode.get("timestamps")).flatMap(timestamps -> Optional.ofNullable(timestamps.get("trashed"))).map(JsonNode::asText).map(OffsetDateTime::parse).orElse(null);
        this.updated = Optional.ofNullable(jsonNode.get("timestamps")).flatMap(timestamps -> Optional.ofNullable(timestamps.get("updated"))).map(JsonNode::asText).map(OffsetDateTime::parse).orElse(null);
        this.userEdited = Optional.ofNullable(jsonNode.get("timestamps")).flatMap(timestamps -> Optional.ofNullable(timestamps.get("userEdited"))).map(JsonNode::asText).map(OffsetDateTime::parse).orElse(null);
        this.labelIds = Stream.ofNullable(jsonNode.get("labelIds"))
                .flatMap(labelIdsNode ->
                        StreamSupport.stream(labelIdsNode.spliterator(), false)
                                // assuming "deleted" is always "1970-01-01T00:00:00.000000Z" and that means never
                                .filter(JsonNode::isObject)
                                .map(element -> element.get("labelId").asText()))
                .collect(Collectors.toList());
        this.checked = Optional.ofNullable(jsonNode.get("checked")).map(JsonNode::asBoolean).orElse(null);

        this.webLinks = extractWebLinks(jsonNode);
        this.imageInfo = extractImageInfo(jsonNode);
        this.drawingInfo = extractDrawingInfo(jsonNode);
    }

    private List<String> extractWebLinks(JsonNode jsonNode) {

        return Optional.ofNullable(jsonNode.get("annotationsGroup"))
                .flatMap(annotationsGroup -> Optional.ofNullable(annotationsGroup.get("annotations")))
                .map(annotationsNode ->
                        StreamSupport.stream(annotationsNode.spliterator(), false)
                                .map(annotationNode -> Optional.ofNullable(annotationNode.get("webLink")))
                                .flatMap(Optional::stream)
                                .map(webLinkNode -> Stream.of(
                                        Optional.ofNullable(webLinkNode.get("title")).map(JsonNode::asText),
                                        Optional.ofNullable(webLinkNode.get("url")).map(JsonNode::asText)/*,
                                        Optional.ofNullable(webLinkNode.get("description")).map(JsonNode::asText)*/)
                                        .flatMap(Optional::stream)
                                        .collect(Collectors.joining("\n")))
                                .collect(Collectors.toList()))
                .orElse(null);
    }

    private String extractImageInfo(JsonNode jsonNode) {

        return Optional.ofNullable(jsonNode.get("blob"))
                .filter(blob -> blob.get("type").asText().equals(BlobType.IMAGE.toString()))
                .map(blob -> {
                    String size = getSizeString(blob.get("byte_size").asInt());
                    String extractedText = blob.get("extracted_text").asText().strip();
                    return String.format("[%s, %s, %dx%d]",
                            blob.get("mimetype").asText(),
                            size,
                            blob.get("width").asInt(), blob.get("height").asInt()) +
                            (extractedText != null && !extractedText.equals("null") ? ("\n" + extractedText) : "");
                })
                .orElse(null);
    }

    private String extractDrawingInfo(JsonNode jsonNode) {

        return Optional.ofNullable(jsonNode.get("blob"))
                .filter(blob -> blob.get("type").asText().equals(BlobType.DRAWING.toString()))
                .map(blob -> {
                    JsonNode snapshotData = blob.get("drawingInfo").get("snapshotData");
                    String size = getSizeString(snapshotData.get("byte_size").asInt());
                    String extractedText = blob.get("extracted_text").asText().strip();
                    return String.format("[%s, %s, %dx%d]",
                            snapshotData.get("mimetype").asText(),
                            size,
                            snapshotData.get("width").asInt(), snapshotData.get("height").asInt()) +
                            (extractedText != null && !extractedText.equals("null") ? ("\n" + extractedText) : "");
                })
                .orElse(null);
    }

    private void appendText(@Nullable String textToAppend) {

        this.text = Stream.of(
                Optional.ofNullable(this.text),
                Optional.ofNullable(textToAppend))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.collectingAndThen(
                        Collectors.joining("\n\n"),
                        text -> text.equals("") ? null : text));
    }

    @NotNull
    private static String getSizeString(int byteSize) {
        return byteSize == 0 ? "??? KiB" :
                byteSize < 1024 * 1024 ? (byteSize / 1024) + " KiB" :
                        BigDecimal.valueOf(byteSize)
                                .divide(BigDecimal.valueOf(1024 * 1024))
                                .setScale(2, RoundingMode.HALF_EVEN) + " MiB";
    }

    boolean appearsEmpty() {

        /*
        return Stream.of(
                this.title,
                this.text,
                this.webLinks,
                this.imageInfo,
                this.drawingInfo)
                .allMatch(Objects::isNull);
         */

        return getContentTypes().isEmpty();
    }

    enum ContentType {text, webLinks, image, drawing}

    Collection<ContentType> getContentTypes() {

        Collection<ContentType> contentTypes = new ArrayList<>();

        if (this.text != null) {
            contentTypes.add(ContentType.text);
        }
        if (this.webLinks != null) {
            contentTypes.add(ContentType.webLinks);
        }
        if (this.imageInfo != null) {
            contentTypes.add(ContentType.image);
        }
        if (this.drawingInfo != null) {
            contentTypes.add(ContentType.drawing);
        }

        return contentTypes;
    }

    Optional<ContentType> getContentType() {

        if (this.getContentTypes().size() > 1) {
            throw new RuntimeException("Unexpected multiple content types");
        }

        return this.getContentTypes().stream().findAny();
    }

    public String toString() {

        return "ID: " + this.id +
                Optional.ofNullable(this.title).map(title -> "\n" + "Title: " + title).orElse("") +
                Optional.ofNullable(this.text).map(text -> "\n" + "Text:" + "\n" + text.indent(8).stripTrailing()).orElse("") +
                Stream.of(this.labelIds).map(labelIds -> labelIds.isEmpty() ? "" : "\n" + "Label IDs: " + labelIds).findFirst().get() +
                Stream.of(Stream.ofNullable(this.webLinks).flatMap(Collection::stream).collect(Collectors.joining("\n\n")))
                        .map(webLinks -> webLinks.equals("") ? "" : "\n" + "Web links:" + "\n" + webLinks.indent(8).stripTrailing()).findFirst().get() +
                Optional.ofNullable(this.imageInfo).map(imageInfo -> "\n" + "Image:" + "\n" + imageInfo.indent(8).stripTrailing()).orElse("") +
                Optional.ofNullable(this.drawingInfo).map(drawingInfo -> "\n" + "Drawing:" + "\n" + drawingInfo.indent(8).stripTrailing()).orElse("");
    }
}
